# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-10-10 09:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PostfixEmail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('recipent_email', models.CharField(blank=True, max_length=200, null=True, verbose_name='Recipent Email')),
                ('subject', models.CharField(blank=True, max_length=200, null=True, verbose_name='subject')),
                ('body', models.TextField(blank=True, null=True, verbose_name='Email Body')),
                ('name', models.CharField(blank=True, max_length=200, null=True, verbose_name='Recipent name')),
                ('MobileNumber', models.CharField(blank=True, max_length=200, null=True, verbose_name='Recipent mobile Numner')),
                ('postfix_email_id', models.CharField(blank=True, max_length=200, null=True, verbose_name='postfix_email_id')),
                ('create_date', models.DateTimeField(auto_now=True, null=True)),
                ('update_status', models.DateTimeField(auto_now=True, null=True)),
                ('status', models.CharField(blank=True, choices=[('sent', 'Sent'), ('Open', 'Open'), ('Delivered', 'Delivered'), ('Bounced', 'Bounced')], help_text='This is set automatically.', max_length=20, verbose_name='Status')),
            ],
        ),
    ]
