from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
EMAIL_STATUS = (
    ('sent', _('Sent')),
    ('Open', _('Open')),
    ('Delivered', _('Delivered')),
    ('Bounced', _('Bounced')),
)
class PostfixEmail(models.Model):
    recipent_email = models.CharField(_("Recipent Email"), max_length=200,blank=True, null=True)
    subject = models.CharField(_("subject"), max_length=200,blank=True, null=True)
    body = models.TextField(_("Email Body") , blank=True, null=True)
    name = models.CharField(_("Recipent name"), max_length=200,blank=True, null=True)
    MobileNumber = models.CharField(_("Recipent mobile Numner"), max_length=200,blank=True, null=True)
    postfix_email_id = models.CharField(_("postfix_email_id"), max_length=200,blank=True, null=True)
    create_date = models.DateTimeField(null=True, blank=True, auto_now=True)
    update_status = models.DateTimeField(null=True, blank=True, auto_now=True)
    status = models.CharField(_("Status"), max_length=20, choices=EMAIL_STATUS,blank=True, help_text=_("This is set automatically."))