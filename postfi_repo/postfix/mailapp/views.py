from django.shortcuts import render,render_to_response
from .models import PostfixEmail
import commands
import re
from django.template import RequestContext
from django.http import HttpResponse

# Create your views here.
def worker(obj,email):
    """thread worker function"""
    import time
    time.sleep(8)
    x = "sudo cat /var/log/mail.log |grep 'status=sent (250 2.0.0 OK'"
    x_bounc = "grep -e 'status=bounced (Host' -e 'status=bounced (host' /var/log/mail.log"
    bounce_email = re.findall(r'[\w\.-]+@[\w\.-]+', commands.getstatusoutput(x_bounc)[1].split('\n')[-1])[0]
    log_email = re.findall(r'[\w\.-]+@[\w\.-]+', commands.getstatusoutput(x)[1].split('\n')[-1])[0]
    if log_email == email:
        mail_id = re.findall(r': (.*?): to=', commands.getstatusoutput(x)[1].split('\n')[-1])[0]
    elif bounce_email == email:
        mail_id = re.findall(r': (.*?): to=', commands.getstatusoutput(x_bounc)[1].split('\n')[-1])[0]
    else:
        mail_id = ''
    obj.postfix_email_id = mail_id
    obj.save()
    return
def postfix_send(request):
    if request.method == 'POST':
        from subprocess import call
        import threading
        formdata = request.POST.copy()
        email = formdata['email']
        subject = formdata['subject']
        body = formdata['body']
        command = '''echo "'''+str(body)+'''" | mail -s "'''+str(subject)+'''" "'''+email+'''"'''
        try:
            success = call(command, shell=True)
            if success == 0:
                p_email = PostfixEmail()
                p_email.name = formdata['name']
                p_email.subject = subject
                p_email.recipent_email = email
                p_email.body = body
                p_email.MobileNumber = formdata['mobile']
                p_email.status = 'sent'
                p_email.save()
                success = "massage sent successfully"
                mail_id =''
                try:
                    threads = []
                    t = threading.Thread(target=worker, args=(p_email,email,))
                    threads.append(t)
                    t.start()
                except:
                    pass
            else:
                success = "There is some error.please contact tech team"
        except:
            success = "There is some error.please contact tech team"
            pass

    template_name = "postfix_send.html"
    return render_to_response(template_name, locals(), context_instance=RequestContext(request))

def postfix_send_email_status(request):
    if request.is_ajax():
        from collections import defaultdict
        bounced_dict = defaultdict(list)
        delivered_dict = defaultdict(list)
        bounced_command = "sudo cat /var/log/mail.log |grep 'status=bounced'"
        delivered_command = "sudo cat /var/log/mail.log |grep 'status=sent (250 2.0.0 OK'"
        """ this is for updating bounced status in table with massage id"""
        try:
            for i in commands.getstatusoutput(bounced_command)[1].split('\n'):
                id=re.findall(r': (.*?): to=',i)[0]
                if PostfixEmail.objects.filter(postfix_email_id=id):
                    pse_obj = PostfixEmail.objects.get(postfix_email_id=id)
                    pse_obj.status = 'Bounced'
                    pse_obj.save()
            """ this is for updating Delivered status in table by massage id"""
            for i in commands.getstatusoutput(delivered_command)[1].split('\n'):
                id = re.findall(r': (.*?): to=', i)[0]
                if PostfixEmail.objects.filter(postfix_email_id=id):
                    pse_obj = PostfixEmail.objects.get(postfix_email_id=id)
                    pse_obj.status = 'Delivered'
                    pse_obj.save()
        except:
            pass
        p_email_all = PostfixEmail.objects.all()
        html = "<table class='table'><thead><tr><th>#</th><th>Name</th><th>Email</th><th>Subject</th><th>Mobile Number</th><th>Create date</th><th>Status</th></tr></thead><tbody>"
        for count,pemail in enumerate(p_email_all):
            html = html+"<tr><td>"+str(count+1)+"</td><td>"+str(pemail.name)+"</td><td>"+str(pemail.recipent_email)+"</td><td>"+str(pemail.subject)+"</td><td>"+str(pemail.MobileNumber)+"</td><td>"+str(pemail.create_date.strftime("%Y-%m-%d %H:%M:%S"))+"</td><td>"+str(pemail.status)+"</td><td></tr>"
        html = html+"</tbody></table>"
        return HttpResponse(html)
    else:
        p_email_all = PostfixEmail.objects.all()
        template_name = "postfix_send_report.html"
        return render_to_response(template_name, locals(), context_instance=RequestContext(request))